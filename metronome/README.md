#AOC METRONOME AOC
*by Augustin Bardou*

##Installation
Step 1: Download the entire project.

Step 2: open a terminal and change the current directory to be in the repository of the project.

Step 3: run the following command to import all dependencies: ***mvn clean install***

Step 4: open the project with your favorite IDE.

##Launch the program
To run the Metronome program, you just have to execute the ***MetronomeApp.java*** file in ***/src/main/java/fr/istic/aoc/metronome/ directory***.

A window will appear and you will be able to use it.

##GUI
The graphical user interface is composed with:

- 2 circles (LED),

- 4 buttons (START, INC, DEC, STOP),

- 1 slider (min = 0, max = 120, init = 60),

- 1 textfield (display the tempo and the rhythm).

###Button START
Start the metronome with a tempo at 60 and a rhythm at 3.

###Button INC
Increase the rhythm by 1 to a maximum of 7.

###Button DEC
Decrease the rhythm by 1 to a minimum of 2.

###Button STOP
Stop the metronome.

###Slider
Move from 0 to 120 to modify the tempo of the metronome.

##Other files
The file ***diagrammeDeClasseV1*** show you how my classes are connected.

The file ***diagrammeSequence*** show you what happen when you click on the button start.