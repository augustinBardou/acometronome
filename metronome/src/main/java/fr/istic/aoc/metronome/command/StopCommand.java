package fr.istic.aoc.metronome.command;

import fr.istic.aoc.metronome.controller.Controller;
import fr.istic.aoc.metronome.util.Command;

/**
 * Created by augustin
 */
public class StopCommand implements Command {

    private Controller controller;

    /**
     * Constructor
     */
    public StopCommand() {
    }

    /**
     * Execute the method stop from the controller
     */
    @Override
    public void execute() {
        controller.stop();
    }

    /**
     * Get the controller
     * @return the controller
     */
    public Controller getController(){
        return controller;
    }

    /**
     * Set the value of controller
     * @param controller
     */
    public void setController(Controller controller) {
        this.controller = controller;
    }
}
