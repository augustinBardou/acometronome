package fr.istic.aoc.metronome.util;

/**
 * Created by augustin
 */
public interface MouseScrollWheel {
    float position();
}
