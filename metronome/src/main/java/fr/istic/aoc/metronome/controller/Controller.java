package fr.istic.aoc.metronome.controller;

import fr.istic.aoc.metronome.model.Model;
import fr.istic.aoc.metronome.util.Clock;
import fr.istic.aoc.metronome.view.MetronomeView;

/**
 * Created by augustin
 */
public interface Controller {
    void start();

    void stop();

    void tempo();

    void rhythm();

    void increaseRhythm();

    void decreaseRhythm();

    void updateTempo(float _tempo);

    void turnOffLED(String _color);

    Model getModel();

    void setModel(Model _model);

    MetronomeView getMetronomeView();

    void setMetronomeView(MetronomeView _metronomeView);

    Clock getClock();

    void setClock(Clock _clock);
}
