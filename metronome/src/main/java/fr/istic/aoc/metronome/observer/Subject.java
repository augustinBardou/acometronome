package fr.istic.aoc.metronome.observer;

/**
 * Created by augustin
 */
public interface Subject {
    void addObserver(Observer o);
    void notifyObservers();
}
