package fr.istic.aoc.metronome.observer;

/**
 * Created by augustin
 */
public interface Observer {
    void update();
}
