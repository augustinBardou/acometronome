package fr.istic.aoc.metronome.util;

/**
 * Created by augustin
 */
public interface Command {
    void execute();
}
