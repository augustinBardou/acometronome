package fr.istic.aoc.metronome.view;

import fr.istic.aoc.metronome.controller.Controller;

/**
 *
 * Created by leiko on 28/09/15.
 */
public interface MetronomeView {

    void setController(Controller _concreteController);

    void onBeat();

    void onMeasure();

    void onClose();

    void onStart();

    void onStop();

    void changeLEDColor(String _color);

    void updateRhythm(int _rhythm);

    void updateTempo(float tempo);

    void playSound(String _soundtrack);
}
