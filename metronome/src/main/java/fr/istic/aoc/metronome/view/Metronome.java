package fr.istic.aoc.metronome.view;

import fr.istic.aoc.metronome.command.StartCommand;
import fr.istic.aoc.metronome.command.StopCommand;
import fr.istic.aoc.metronome.controller.Controller;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Slider;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;

import javax.sound.sampled.*;
import java.io.IOException;
import java.net.URL;

/**
 * Created by augustin
 */
public class Metronome implements MetronomeView {

    private Controller controller;
    private static final int TEMPO_MIN = 0;
    private static final int TEMPO_MAX = 120;
    private static final int TEMPO_INIT = 60;
    @FXML private Circle redLED;
    @FXML private Circle greenLED;
    @FXML private Text tempoTxt;
    @FXML private Text rhythmTxt;
    @FXML private Slider tempoSlider;
    private Clip clip;
    private URL soundtrackKick;
    private URL soundtrackSnare;
    private AudioInputStream audioInputStream;

    /**
     * Initiate the metronome calling the method execute from the commandStart object
     * @param event
     */
    @FXML protected void startButton(ActionEvent event){
        StartCommand commandStart = new StartCommand();
        commandStart.setController(controller);
        commandStart.execute();
    }

    /**
     * Call the method increaseRhythm from the controller
     * @param event
     */
    @FXML protected void increaseRhythmButton(ActionEvent event){
        controller.increaseRhythm();
    }

    /**
     * Call the method decreaseRhythm from the controller
     * @param event
     */
    @FXML protected void decreaseRhythmButton(ActionEvent event){
        controller.decreaseRhythm();
    }

    /**
     * Stop the metronome calling the method execute from the StopCommand
     * @param event
     */
    @FXML protected void stopButton(ActionEvent event){
        StopCommand commandStop = new StopCommand();
        commandStop.setController(controller);
        commandStop.execute();
    }

    /**
     * Constructor
     */
    public Metronome() {
    }

    /**
     * Set a new value for the controller
     * @param _concreteController
     */
    @Override
    public void setController(Controller _concreteController) {
        controller = _concreteController;
    }

    /**
     * unimplemented
     */
    @Override
    public void onBeat() {

    }
    /**
     * unimplemented
     */
    @Override
    public void onMeasure() {

    }

    /**
     * System.exit(0)
     */
    @Override
    public void onClose() {
        System.exit(0);
    }

    /**
     * Set up the slider and add it a Listener
     */
    @Override
    public void onStart() {
        soundtrackKick = this.getClass().getClassLoader().getResource("kick2.wav");
        soundtrackSnare = this.getClass().getClassLoader().getResource("snare2.wav");
        try {
            clip = AudioSystem.getClip();
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }
        tempoSlider.setMin(TEMPO_MIN);
        tempoSlider.setMax(TEMPO_MAX);
        tempoSlider.setValue(TEMPO_INIT);
        updateTempo(TEMPO_INIT);
        updateRhythm(controller.getModel().getRhythmTemps());
        tempoSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            controller.getModel().setTempo(newValue.longValue());
            updateTempo(newValue.longValue());
        });
    }
    /**
     * unimplemented
     */
    @Override
    public void onStop() {

    }

    /**
     * Change the color of the LED (4 choices: red, redOff, green, greenOff)
     * @param _color
     */
    @Override
    public void changeLEDColor(String _color) {
        if(_color.equals("red")){
            redLED.setFill(Color.RED);
        }
        if(_color.equals("green")){
            greenLED.setFill(Color.GREEN);
        }
        if(_color.equals("redOff")){
            redLED.setFill(Color.web("#c61a1a"));
        }
        if(_color.equals("greenOff")){
            greenLED.setFill(Color.web("#29b532"));
        }
    }

    /**
     * Set a new value for the text of the rhythm
     * @param _rhythm
     */
    @Override
    public void updateRhythm(int _rhythm) {
        rhythmTxt.setText("Rhythm: "+ _rhythm);
    }

    /**
     * Set a new value for the text of the tempo
     * @param tempo
     */
    @Override
    public void updateTempo(float tempo) {
        tempoTxt.setText("Tempo: "+ tempo);
    }

    /**
     * Get the slider
     * @return the slider
     */
    public Slider getTempoSlider() {
        return tempoSlider;
    }

    /**
     * Play a sound
     * @param _soundtrack name of the song in string
     */
    public void playSound(String _soundtrack){
        try {
            audioInputStream = null;
            if (clip.isRunning()){
                clip.stop();
            }
            if(clip.isOpen()){
                clip.close();
            }
            if(_soundtrack.equals("kick")){
                audioInputStream = AudioSystem.getAudioInputStream(soundtrackKick);
            }
            if(_soundtrack.equals("snare")){
                audioInputStream = AudioSystem.getAudioInputStream(soundtrackSnare);
            }
            clip.open(audioInputStream);
            clip.start();
            audioInputStream.close();
        } catch (UnsupportedAudioFileException | IOException | LineUnavailableException e) {
            e.printStackTrace();
        }
    }
}
