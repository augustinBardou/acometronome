package fr.istic.aoc.metronome.model;

import fr.istic.aoc.metronome.command.TickCommand;
import fr.istic.aoc.metronome.command.TurnOffLEDCommand;
import fr.istic.aoc.metronome.controller.Controller;
import fr.istic.aoc.metronome.observer.Observer;
import fr.istic.aoc.metronome.observer.Subject;
import fr.istic.aoc.metronome.util.Clock;
import fr.istic.aoc.metronome.util.Command;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by augustin
 */
public class ConcreteModel implements Model, Subject{

    private Controller controller;
    private Clock clock;
    private boolean state;
    private float tempo;
    private int period;
    private int currentPeriod;
    private Command tempoCommand;
    private Command rhythmCommand;
    private Command tickCommand;
    private List<Observer> observerList;

    /**
     * Constructor
     */
    public ConcreteModel() {
        state = false;
        tempo = 60;
        period = 3;
        currentPeriod = 1;
        tickCommand = new TickCommand(this);
        observerList = new ArrayList<>();
    }

    /**
     * Tell when the LED need to be turn on
     */
    @Override
    public void tick() {
        if(currentPeriod != period){
            tempoCommand.execute();
            currentPeriod++;
            TurnOffLEDCommand turnOnLED = new TurnOffLEDCommand();
            turnOnLED.setController(controller);
            turnOnLED.setColor("green");
            turnOnLED.execute();
            System.out.println("tick currentPeriod: "+currentPeriod+"\t\t--\t\t tick period: "+period);
        }
        else{
            rhythmCommand.execute();
            TurnOffLEDCommand turnOnLED = new TurnOffLEDCommand();
            turnOnLED.setController(controller);
            turnOnLED.setColor("red");
            turnOnLED.execute();
            currentPeriod = 1;
        }
    }

    /**
     * Get the state
     * @return the state
     */
    @Override
    public boolean getState() {
        return state;
    }

    /**
     * Set a new value for the state and set the clock
     * @param _state
     */
    @Override
    public void setState(boolean _state) {
        if(state != _state){
            state = _state;
            currentPeriod = 1;
            if(state){
                if(clock == null){
                    clock = new ConcreteClock();
                    controller.setClock(clock);
                }
                clock.activatePeriodically(tickCommand, (60/tempo));
            }
            else{
                if(this.clock != null){
                    clock.deactivate();
                }
            }
            notifyObservers();
        }
    }

    /**
     * Restart
     */
    @Override
    public void restart() {
        setState(false);
        setState(true);
    }

    /**
     * Get the tempo
     * @return the tempo
     */
    @Override
    public float getTempo() {
        return tempo;
    }

    /**
     * Set a new value for the tempo and call restart
     * @param _tempo
     */
    @Override
    public void setTempo(float _tempo) {
        tempo = _tempo;
        notifyObservers();
        if(getState()){
            restart();
        }
    }

    /**
     * Get the period (period = rhythm)
     * @return the period
     */
    @Override
    public int getRhythmTemps() {
        return period;
    }

    /**
     * Set a new value for the period (period = rhythm) and it must be between 2 and 7
     * @param _RhythmTemps
     */
    @Override
    public void setRhythmTemps(int _RhythmTemps) {
        if((period >= 2) && (period <= 7)){
            period = _RhythmTemps;
            notifyObservers();
        }
        if(period <= 2)
        {
            period = 2;
            notifyObservers();
        }
        if(period >= 7){
            period = 7;
            notifyObservers();
        }
    }

    /**
     * Get the tempoCommand
     * @return the tempoCommand
     */
    @Override
    public Command getCommandTempo() {
        return tempoCommand;
    }

    /**
     * Set a new value for the tempoCommand
     * @param _commandTempo
     */
    @Override
    public void setCommandTempo(Command _commandTempo) {
        tempoCommand = _commandTempo;
    }

    /**
     * Get the rhythmCommand
     * @return rhythmCommand
     */
    @Override
    public Command getCommandRhythm() {
        return rhythmCommand;
    }

    /**
     * Set a new value for the rhythmCommand
     * @param _CommandRhythm
     */
    @Override
    public void setCommandRhythm(Command _CommandRhythm) {
        rhythmCommand = _CommandRhythm;
    }

    /**
     * Get the controller
     * @return the controller
     */
    @Override
    public Controller getController() {
        return controller;
    }

    /**
     * Set a new value for the controller and add it to the observerList
     * @param _controller
     */
    @Override
    public void setController(Controller _controller) {
        controller = _controller;
        addObserver((Observer)controller);
    }

    /**
     * Add an observer to the observerList
     * @param _observer
     */
    @Override
    public void addObserver(Observer _observer) {
        observerList.add(_observer);
    }

    /**
     * Call the method update() on every observer contained in the observerList
     */
    @Override
    public void notifyObservers() {
        for(Observer obs : observerList){
            obs.update();
        }
    }

    /**
     * Get the clock
     * @return the clock
     */
    public Clock getClock(){
        return clock;
    }

    /**
     * Set a new value for the clock
     * @param _clock
     */
    public void setClock(Clock _clock){
        clock = _clock;
    }

    /**
     * Get the observerList
     * @return the observerList
     */
    public List<Observer> getObserverList() {
        return observerList;
    }
}
