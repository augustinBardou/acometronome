package fr.istic.aoc.metronome.command;

import fr.istic.aoc.metronome.controller.Controller;
import fr.istic.aoc.metronome.util.Command;

/**
 * Created by augustin
 */
public class RhythmCommand implements Command {

    private Controller controller;

    /**
     * Constructor
     * @param _controller
     */
    public RhythmCommand(Controller _controller) {
        controller = _controller;
    }

    /**
     * Call the method rhythm from the controller
     */
    @Override
    public void execute() {
        controller.rhythm();
    }

    /**
     * Get the controller
     * @return the controller
     */
    public Controller getController(){
        return controller;
    }
}
