package fr.istic.aoc.metronome.command;

import fr.istic.aoc.metronome.util.Command;

import java.util.TimerTask;

/**
 * Created by augustin
 */
public class TaskClock extends TimerTask{
    Command command;

    /**
     * Constructor
     */
    public TaskClock() {
    }

    /**
     * Run the method execute of the command
     */
    @Override
    public void run() {
        command.execute();
    }

    /**
     * Get the command
     * @return the command
     */
    public Command getCommand() {
        return command;
    }

    /**
     * Set the value of the command
     * @param command
     */
    public void setCommand(Command command) {
        this.command = command;
    }
}
