package fr.istic.aoc.metronome;

import fr.istic.aoc.metronome.model.ConcreteModel;
import fr.istic.aoc.metronome.model.Model;
import fr.istic.aoc.metronome.controller.ConcreteController;
import fr.istic.aoc.metronome.controller.Controller;
import fr.istic.aoc.metronome.view.Metronome;
import fr.istic.aoc.metronome.view.MetronomeView;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Metronome App
 *
 */
public class MetronomeApp extends Application {

    private MetronomeView metronome;

    public static void main(String[] args) {
        // launch JavaFX application
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        Model model = new ConcreteModel();
        Controller controller = new ConcreteController();
        controller.setModel(model);
        model.setController(controller);
        metronome = new Metronome();
        metronome.setController(controller);
        System.out.println("load UI");
        FXMLLoader loader = new FXMLLoader();
        Parent root = loader.load(getClass().getResource("/metronome.fxml").openStream());
        metronome = loader.getController();
        controller.setMetronomeView(metronome);
        metronome.setController(controller);
        metronome.onStart();
        stage.setTitle("Metronome");
        stage.setScene(new Scene(root, 450, 270));
        stage.show();
    }

    @Override
    public void stop() throws Exception {
        metronome.onClose();
        System.out.println("quit");
    }
}
