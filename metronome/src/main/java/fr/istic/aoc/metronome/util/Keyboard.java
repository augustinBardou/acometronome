package fr.istic.aoc.metronome.util;

/**
 * Created by augustin
 */
public interface Keyboard {
    boolean keyPressed(int _key);
}
