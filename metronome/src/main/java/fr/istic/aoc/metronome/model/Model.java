package fr.istic.aoc.metronome.model;

import fr.istic.aoc.metronome.observer.Observer;
import fr.istic.aoc.metronome.controller.Controller;
import fr.istic.aoc.metronome.util.Command;

/**
 * Created by augustin
 */
public interface Model {

    void tick();

    boolean getState();

    void setState(boolean _state);

    void restart();

    float getTempo();

    void setTempo(float _tempo);

    int getRhythmTemps();

    void setRhythmTemps(int _RhythmTemps);

    Command getCommandTempo();

    void setCommandTempo(Command _commandTempo);

    Command getCommandRhythm();

    void setCommandRhythm(Command _CommandRhythm);

    Controller getController();

    void setController(Controller _controller);

    void addObserver(Observer _observer);

    void notifyObservers();
}
