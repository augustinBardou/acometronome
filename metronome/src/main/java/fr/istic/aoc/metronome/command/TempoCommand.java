package fr.istic.aoc.metronome.command;

import fr.istic.aoc.metronome.controller.Controller;
import fr.istic.aoc.metronome.util.Command;

/**
 * Created by augustin
 */
public class TempoCommand implements Command {

    private Controller controller;

    /**
     * Constructor
     * @param _controller
     */
    public TempoCommand(Controller _controller) {
        controller = _controller;
    }

    /**
     * Execute the method tempo of the controller
     */
    @Override
    public void execute() {
        controller.tempo();
    }

    /**
     * Get the controller
     * @return controller
     */
    public Controller getController(){
        return controller;
    }
}
