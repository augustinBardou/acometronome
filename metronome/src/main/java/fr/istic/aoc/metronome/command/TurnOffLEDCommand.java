package fr.istic.aoc.metronome.command;

import fr.istic.aoc.metronome.controller.Controller;
import fr.istic.aoc.metronome.util.Command;

/**
 * Created by augustin
 */
public class TurnOffLEDCommand implements Command{

    private Controller controller;
    private String color;

    /**
     * Constructor
     */
    public TurnOffLEDCommand() {
    }

    /**
     * Execute the method turnOffLED(String _color) from the controller
     */
    @Override
    public void execute() {
        controller.turnOffLED(color);
    }

    /**
     * Get the controller
     * @return controller
     */
    public Controller getController(){
        return controller;
    }

    /**
     * Set a new value for the controller
     * @param controller
     */
    public void setController(Controller controller) {
        this.controller = controller;
    }

    /**
     * Get the color
     * @return color
     */
    public String getColor() {
        return color;
    }

    /**
     * Set a new value for the color
     * @param color
     */
    public void setColor(String color) {
        this.color = color;
    }
}
