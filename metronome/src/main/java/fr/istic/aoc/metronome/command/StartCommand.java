package fr.istic.aoc.metronome.command;

import fr.istic.aoc.metronome.controller.ConcreteController;
import fr.istic.aoc.metronome.controller.Controller;
import fr.istic.aoc.metronome.util.Command;

/**
 * Created by augustin
 */
public class StartCommand implements Command {

    private Controller controller;

    /**
     * Constructor
     */
    public StartCommand() {
        controller = new ConcreteController();
    }

    /**
     * Execute the method start from the controller
     */
    @Override
    public void execute() {
        controller.start();
    }

    /**
     * Get the controller
     * @return the controller
     */
    public Controller getController(){
        return controller;
    }

    /**
     * Set the value of the controller
     * @param _controller
     */
    public void setController(Controller _controller) {
        controller = _controller;
    }
}
