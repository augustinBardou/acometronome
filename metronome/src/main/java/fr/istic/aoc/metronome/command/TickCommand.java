package fr.istic.aoc.metronome.command;

import fr.istic.aoc.metronome.model.Model;
import fr.istic.aoc.metronome.util.Command;

/**
 * Created by augustin
 */
public class TickCommand implements Command {

    private Model model;

    /**
     * Constructor
     * @param _model
     */
    public TickCommand(Model _model) {
        model = _model;
    }

    /**
     * Execute the method tick of the model
     */
    @Override
    public void execute() {
        model.tick();
    }

    /**
     * Get the model
     * @return model
     */
    public Model getModel(){
        return model;
    }
}
