package fr.istic.aoc.metronome.util;

/**
 * Created by augustin
 */
public interface Display {
    void turnOnLED(int _numLED);
    void turnOffLEF(int _numLED);
}
