package fr.istic.aoc.metronome.controller;

import fr.istic.aoc.metronome.model.Model;
import fr.istic.aoc.metronome.observer.Observer;
import fr.istic.aoc.metronome.command.RhythmCommand;
import fr.istic.aoc.metronome.command.TempoCommand;
import fr.istic.aoc.metronome.command.TurnOffLEDCommand;
import fr.istic.aoc.metronome.util.Clock;
import fr.istic.aoc.metronome.view.MetronomeView;

/**
 * Created by augustin
 */
public class ConcreteController implements Controller, Observer{

    private Model model;
    private Clock clock;
    private TurnOffLEDCommand turnOffLEDCommand;
    private MetronomeView metronomeView;

    /**
     * Constructor
     */
    public ConcreteController() {
        model = null;
        clock = null;
        metronomeView = null;
        turnOffLEDCommand = new TurnOffLEDCommand();
        turnOffLEDCommand.setController(this);
    }

    /**
     * Call the method setState from the model to set it at true
     */
    @Override
    public void start() {
        model.setState(true);
    }
    /**
     * Call the method setState from the model to set it at false
     */
    @Override
    public void stop() {
        model.setState(false);
    }

    /**
     * Set the color of turnOffLEDCommand and call the method activateAfterDelay from the clock to make the LED blink
     */
    @Override
    public void tempo() {
        turnOffLEDCommand.setColor("greenOff");
        metronomeView.playSound("kick");
        clock.activateAfterDelay(turnOffLEDCommand,(float)0.5);
    }

    /**
     * Set the color of turnOffLEDCommand and call the method activateAfterDelay from the clock to make the LED blink
     */
    @Override
    public void rhythm() {
        turnOffLEDCommand.setColor("redOff");
        metronomeView.playSound("snare");
        clock.activateAfterDelay(turnOffLEDCommand, (float)0.5);
    }

    /**
     * Increase the rhythm from the model
     */
    @Override
    public void increaseRhythm() {
        model.setRhythmTemps(model.getRhythmTemps()+1);
    }

    /**
     * Decrease the rhythm from the model
     */
    @Override
    public void decreaseRhythm() {
        model.setRhythmTemps(model.getRhythmTemps()-1);
    }

    /**
     * Call the method setTempo of the model with _tempo as parameter
     * @param _tempo
     */
    @Override
    public void updateTempo(float _tempo) {
        model.setTempo(_tempo);
    }

    /**
     * Call the method changeLEDColor of the metronome to change the color of the LED on the UI
     * @param _color
     */
    @Override
    public void turnOffLED(String _color) {
        metronomeView.changeLEDColor(_color);
    }

    /**
     * Get the model
     * @return the model
     */
    @Override
    public Model getModel() {
        return model;
    }

    /**
     * Set a new value for the model and set the RhythmCommand and the TempoCommand for the new model
     * @param _model
     */
    @Override
    public void setModel(Model _model) {
        model = _model;
        model.setCommandRhythm(new RhythmCommand(this));
        model.setCommandTempo(new TempoCommand(this));
    }

    /**
     * Get the metronome
     * @return the metronome
     */
    @Override
    public MetronomeView getMetronomeView() {
        return metronomeView;
    }

    /**
     * Set a new value for the metronome
     * @param _metronomeView
     */
    @Override
    public void setMetronomeView(MetronomeView _metronomeView) {
        metronomeView = _metronomeView;
    }

    /**
     * Get the clock
     * @return the clock
     */
    @Override
    public Clock getClock() {
        return clock;
    }

    /**
     * Set a new value for the clock
     * @param _clock
     */
    @Override
    public void setClock(Clock _clock) {
        clock = _clock;
    }

    /**
     * Call the method updateTempo and updateRhythm from the metronome with the value of the tempo and of the rhythm
     */
    @Override
    public void update() {
        metronomeView.updateTempo(model.getTempo());
        metronomeView.updateRhythm(model.getRhythmTemps());
    }
}
