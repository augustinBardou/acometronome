package fr.istic.aoc.metronome.model;

import fr.istic.aoc.metronome.command.TaskClock;
import fr.istic.aoc.metronome.util.Clock;
import fr.istic.aoc.metronome.util.Command;

import java.util.Timer;

/**
 * Created by augustin
 */
public class ConcreteClock implements Clock {

    private Timer timer;
    private TaskClock timerTask;

    /**
     * Constructor
     */
    public ConcreteClock() {
        timer = new Timer();
    }

    /**
     * Set a new timer for the clock to reset the countdown
     * @param _cmd
     * @param _periodSecond
     */
    @Override
    public void activatePeriodically(Command _cmd, float _periodSecond) {
        timerTask = new TaskClock();
        timerTask.setCommand(_cmd);
        timer = new Timer();
        timer.schedule(timerTask, 0, (long)(_periodSecond * 1000));
    }

    /**
     * Set the timer to activate the modification of the LED
     * @param _cmd
     * @param _periodSecond
     */
    @Override
    public void activateAfterDelay(Command _cmd, float _periodSecond) {
        timerTask = new TaskClock();
        timerTask.setCommand(_cmd);
        timer.schedule(timerTask, (long)(_periodSecond * 1000));
    }

    /**
     * Deactivate the clock
     */
    @Override
    public void deactivate() {
        timer.cancel();
    }
}
