package fr.istic.aoc.metronome.util;

/**
 * Created by augustin
 */
public interface Clock {
    void activatePeriodically(Command _cmd, float _periodSecond);

    void activateAfterDelay(Command _cmd, float _periodSecond);

    void deactivate();
}
