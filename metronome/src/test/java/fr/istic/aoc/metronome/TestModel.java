package fr.istic.aoc.metronome;

import fr.istic.aoc.metronome.controller.ConcreteController;
import fr.istic.aoc.metronome.model.ConcreteModel;
import fr.istic.aoc.metronome.observer.Observer;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by augustin
 */
public class TestModel {
    private ConcreteModel concreteModel;
    private ConcreteController concreteController;
    @Before
    public void beforeTest(){
        concreteModel = new ConcreteModel();
        concreteController = new ConcreteController();
    }

    /**
     * Tests the "getTempo" method
     *
     * @see ConcreteModel#getTempo()
     * @type Functional
     * @oracle We must get the tempo with a value at 60.
     * @passed Yes
     */
    @Test
    public void testGetTempo(){
        assertTrue(concreteModel.getTempo()==60);
    }
    /**
     * Tests the "setTempo" method with a float.
     *
     * @see ConcreteModel#setTempo(float)
     * @type Functional
     * @input float _tempo at 30
     * @oracle We must get a new value for the tempo.
     * @passed Yes
     */
    @Test
    public void testSetTempo(){
        concreteModel.setTempo(30);
        assertTrue(concreteModel.getTempo() == 30);
    }
    /**
     * Tests the "getRhythmTemps" method
     *
     * @see ConcreteModel#getRhythmTemps()
     * @type Functional
     * @oracle We must get the rhythm with a value at 3.
     * @passed Yes
     */
    @Test
    public void testGetRhythm(){
        assertTrue(concreteModel.getRhythmTemps()==3);
    }
    /**
     * Tests the "setRhythmTemps" method
     *
     * @see ConcreteModel#setRhythmTemps(int)
     * @type Functional
     * @input int _rhythm at 1 then at 2
     * @oracle We must get the rhythm with a new value at 2.
     * @passed Yes
     */
    @Test
    public void testSetRhythm(){
        concreteModel.setRhythmTemps(1);
        assertFalse(concreteModel.getRhythmTemps() == 1);
        concreteModel.setRhythmTemps(2);
        assertTrue(concreteModel.getRhythmTemps() == 2);
    }
    /**
     * Tests the "addObserver" method
     *
     * @see ConcreteModel#addObserver(Observer)
     * @type Functional
     * @input (Observer)ConcreteController concreteController
     * @oracle We must get a list of observer with 1 element and it must be the controller.
     * @passed Yes
     */
    @Test
    public void testAddObserver(){
        int observerListSize = concreteModel.getObserverList().size();
        concreteModel.setController(concreteController);
        List<Observer> observerArrayList = concreteModel.getObserverList();
        assertTrue(observerArrayList.get(0) == (Observer)concreteController);
        assertTrue(observerListSize + 1 == observerArrayList.size());
    }
}
